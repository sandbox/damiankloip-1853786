<?php

/**
 * @file
 * Contains \Drupal\action\Plugin\Core\Action\Message.
 */

namespace Drupal\action\Plugin\Core\Action;

use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Action\ActionPluginBase;

/**
 * @Plugin(
 *  id = "message"
 * )
 */
class Message extends ActionPluginBase {

  /**
   * Implements \Drupal\action\Plugin\ActionPluginBase::execute().
   */
  public function execute() {
    $message = token_replace(filter_xss_admin($this->configuration['message']), $this->configuration['parameters']);
    drupal_set_message($message);
  }

}
