<?php

/**
 * @file
 * Contains \Drupal\action\Plugin\Core\Action\Email.
 */

namespace Drupal\action\Plugin\Core\Action;

use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Action\ActionPluginBase;

/**
 * @Plugin(
 *  id = "send_email"
 * )
 */
class Email extends ActionPluginBase {

  /**
   * Implements \Drupal\action\Plugin\ActionPluginBase::execute().
   */
  public function execute() {
    $recipient = token_replace($this->configuration['recipient'], $this->configuration['parameters']);

    // If the recipient is a registered user with a language preference, use
    // the recipient's preferred language. Otherwise, use the system default
    // language.
    $recipient_account = user_load_by_mail($recipient);
    if ($recipient_account) {
      $langcode = user_preferred_langcode($recipient_account);
    }
    else {
      $langcode = language_default()->langcode;
    }

    $params = array('context' => $this->configuration);

    if (drupal_mail('system', 'action_send_email', $recipient, $langcode, $params)) {
      watchdog('action', 'Sent email to %recipient', array('%recipient' => $recipient));
    }
    else {
      watchdog('error', 'Unable to send email to %recipient', array('%recipient' => $recipient));
    }
  }

}
