<?php

/**
 * @file
 * Contains \Drupal\action\Plugin\Core\Action\Goto.
 */

namespace Drupal\action\Plugin\Core\Action;

use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Action\ActionPluginBase;

/**
 * @Plugin(
 *  id = "drupal_goto",
 *  parameters = {
 *    "url" = {
 *      "type" = "string",
 *      "label" = @Translation("Destination URL")
 *    }
 *  }
 * )
 */
class DrupalGoto extends ActionPluginBase {

  /**
   * Implements \Drupal\action\Plugin\ActionPluginBase::execute().
   */
  public function execute() {
    drupal_goto(token_replace($this->configuration['url'], $this->configuration['parameters']));
  }

}
