<?php

/**
 * @file
 * Contains \Drupal\Core\Processor\ProcessorManager.
 */

namespace Drupal\Core\Processor;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Plugin\Discovery\ProcessDecorator;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;

/**
 * @todo.
 */
class ProcessorManager extends PluginManagerBase {

  /**
   * Constructs aa ProcessorManager object.
   */
  public function __construct() {
    $this->discovery = new AnnotatedClassDiscovery('Core', 'Processor');
    $this->discovery = new AlterDecorator($this->discovery, 'processor_info');
    $this->discovery = new ProcessDecorator($this->discovery, array($this, 'processDefinition'));
    $this->discovery = new CacheDecorator($this->discovery, 'processor', 'cache');

    $this->factory = new DefaultFactory($this);
  }

}
