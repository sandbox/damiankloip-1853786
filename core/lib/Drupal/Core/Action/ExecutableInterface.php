<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ExecutableInterface.
 */

namespace Drupal\Core\Action;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * @todo.
 */
interface ExecutableInterface extends PluginInspectionInterface {

  /**
   * Gets a parameter definition.
   */
  public function getParameter($key);

  /**
   * Gets all parameter definitions.
   */
  public function getParameters();

  /**
   * Sets a parameter value.
   */
  public function setParameterValue($key, $value);

  /**
   * Gets a parameter value.
   */
  public function getParameterValue($key);

  /**
   * Gets all parameter values.
   */
  public function getParameterValues();

  /**
   * Sets the processor plugin.
   */
  public function setProcessor($plugin_id, array $configuration);

  /**
   * Gets an array of processors.
   */
  public function getProcessors();

  /**
   * Executes the action.
   */
  public function execute();

  /**
   * Executes the action by parameters.
   */
  //public function executeByParameters();

}
