<?php

/**
 * Contains \Drupal\Core\Action\ActionInterface.
 */

namespace Drupal\Core\Action;

/**
 * Interface for action plugins.
 */
interface ActionInterface extends ExecutableInterface {

  /**
   * Executes the action.
   */
  //public function executeByArguments($arguments, $state);

}