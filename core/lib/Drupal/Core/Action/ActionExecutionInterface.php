<?php

/**
 * @file
 * Contains Drupal\Core\Action\ActionExecutionInterface.
 */

namespace Drupal\Core\Action;

interface ActionExecutionInterface {

  /**
   * @todo.
   */
  public function setExecutionState(ExecutionState $state);

  /**
   * @todo.
   */
  public function execute(ActionInterface $action);

}