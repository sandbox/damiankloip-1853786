<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ActionPluginBase.
 */

namespace Drupal\Core\Action;

/**
 * @todo.
 */
abstract class ActionPluginBase extends ExecutablePluginBase implements ActionInterface {

  /**
   * Overrides \Drupal\Core\Action\ExecutablePluginBase::execute().
   */
  public function execute() {
    parent::execute();
  }

}
