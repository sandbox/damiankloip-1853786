<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ActionManager.
 */

namespace Drupal\Core\Action;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Plugin\Discovery\ProcessDecorator;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;

/**
 * @todo.
 */
class ActionManager extends PluginManagerBase {

  /**
   * Constructs aa ActionManager object.
   */
  public function __construct() {
    $this->discovery = new AnnotatedClassDiscovery('Core', 'Action');
    $this->discovery = new AlterDecorator($this->discovery, 'action_info');
    $this->discovery = new ProcessDecorator($this->discovery, array($this, 'processDefinition'));
    $this->discovery = new CacheDecorator($this->discovery, 'action', 'cache');

    $this->factory = new DefaultFactory($this);
  }

  /**
   * Overrides Drupal\Component\Plugin\PluginManagerBase::processDefinition().
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    $definition += array(
      'parameters' => array(),
    );
  }

}
