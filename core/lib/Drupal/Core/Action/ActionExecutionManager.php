<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ActionExecutionManager.
 */

namespace Drupal\Core\Action;

/**
 * @todo.
 */
class ActionExecutionManager implements ActionExecutionInterface {

  /**
   * @todo
   *
   * @var ExecutionState $state
   */
  protected $state;

  public function __construct(ExecutionStateInterface $state) {
    $this->setExecutionState($state);
  }

  /**
   * @todo.
   *
   * @param ExecutionState $state
   */
  public function setExecutionState(ExecutionStateInterface $state) {
    $this->state = $state;
  }

  /**
   * @todo.
   *
   * @param Drupal\Core\Action\ActionInterface $action
   */
  public function execute(ActionInterface $action) {
    // Execute and then care about saving.
    $action->executeByArguments($action->getParameterValues(), $this->state);

    $this->state->runPostExecutionTasks();
  }

}
