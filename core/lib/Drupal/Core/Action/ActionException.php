<?php
/**
 * @file
 * Definition of \Drupal\Core\Action\PluginException.
 */

namespace Drupal\Core\Action;

use Exception;
use Drupal\Component\Plugin\Exception\ExceptionInterface;

/**
 * Generic action exception class.
 */
class ActionException extends Exception implements ExceptionInterface {
}
