<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ExecutablePluginBase.
 */

namespace Drupal\Core\Action;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Processor\ProcessorManager;

/**
 * @todo.
 */
abstract class ExecutablePluginBase extends PluginBase implements ExecutableInterface {

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::getParameter().
   */
  public function getParameter($key) {
    $definition = $this->getDefinition();
    return isset($definition['parameters'][$key]) ? $definition['parameters'][$key] : NULL;
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::getParameters().
   */
  public function getParameters() {
    $definition = $this->getDefinition();
    return $definition['parameters'];
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::setParameterValue().
   */
  public function setParameterValue($key, $value) {
    $definition = $this->getDefinition();

    if (!isset($definition['parameters'][$key])) {
      throw new ActionException("The $key parameter is not valid for this action.");
    }

    $this->configuration['parameters'][$key] = $value;

    return $this;
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::getParameterValue().
   */
  public function getParameterValue($key) {
    $definition = $this->getDefinition();
    if (!isset($definition['parameters'][$key])) {
      throw new ActionException("The $key parameter is not valid for this action.");
    }

    return $this->configuration['parameters'][$key];
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::getParameterValues().
   */
  public function getParameterValues() {
    return $this->configuration['parameters'];
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::setProcessor().
   */
  public function setProcessor($plugin_id, array $configuration) {
    // @todo Change this.
    $manager = new ProcessorManager();
    // @todo Do we want to check this before actually assigning?
    $this->configuration['processors'][] = $manager->createInstance($plugin_id, $configuration);
    return $this;
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::getProcessor().
   */
  public function getProcessors() {
    return $this->configuration['processors'];
  }

  /**
   * Gets all configuration values.
   *
   * @return array
   */
  public function getConfigurationValues() {
    return $this->configuration;
  }

  /**
   * Implements \Drupal\Core\Action\ExecutableInterface::execute().
   */
  public function execute() {
  }

}
