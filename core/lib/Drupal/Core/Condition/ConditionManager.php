<?php

/**
 * @file
 * Contains \Drupal\Core\Condition\ConditionManager.
 */

namespace Drupal\Core\Condition;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Plugin\Discovery\ProcessDecorator;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;

/**
 * @todo.
 */
class ConditionManager extends PluginManagerBase {

  /**
   * Constructs aa ConditionManager object.
   */
  public function __construct() {
    $this->discovery = new AnnotatedClassDiscovery('Core', 'Condition');
    $this->discovery = new AlterDecorator($this->discovery, 'condition_info');
    $this->discovery = new ProcessDecorator($this->discovery, array($this, 'processDefinition'));
    $this->discovery = new CacheDecorator($this->discovery, 'condition', 'cache');

    $this->factory = new DefaultFactory($this);
  }

}
