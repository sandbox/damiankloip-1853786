<?php

/**
 * @file
 * Contains \Drupal\Core\Condition\ConditionInterface.
 */

namespace Drupal\Core\Condition;

use Drupal\Core\Action\ExecutableInterface;

interface ConditionInterface extends ExecutableInterface {
}
