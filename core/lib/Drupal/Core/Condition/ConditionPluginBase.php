<?php

/**
 * @file
 * Contains \Drupal\Core\Action\ConditionPluginBase.
 */

namespace Drupal\Core\Condition;

use Drupal\Core\Action\ExecutablePluginBase;

/**
 * @todo.
 */
abstract class ConditionPluginBase extends ExecutablePluginBase implements ConditionInterface {

  /**
   * Overrides \Drupal\Core\Action\ExecutablePluginBase::execute().
   */
  public function execute() {
    parent::execute();
  }

}
